package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;


@EqualsAndHashCode
@Setter
@Getter
public class DBManager {


    private static DBManager instance = new DBManager();
    private final String URL = "jdbc:derby:memory:testdb;create=true";

    public static synchronized DBManager getInstance() {
        if (instance == null){
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {

        List<User> userList = new ArrayList<>();

        try(Connection connection = DriverManager.getConnection(URL);
            ResultSet resultSet = connection.createStatement().executeQuery(DBRequest.SELECT_ALL_USERS)) {
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String login = resultSet.getString(2);
                User user = new User(login);
                user.setId(id);
                userList.add(user);
            }

        } catch (SQLException e) {
            throw new DBException("ERROR", e);
        }
        return userList;
    }

    public boolean insertUser(User user) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int isInsert;

        try {
            connection = DriverManager.getConnection(URL);
            preparedStatement = connection.prepareStatement(DBRequest.INSERT_INTO_USERS_LOGIN_VALUES,Statement.RETURN_GENERATED_KEYS);
            connection.setTransactionIsolation (Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit (false);
            preparedStatement.setString(1,user.getLogin());
            isInsert = preparedStatement.executeUpdate();
            if (isInsert > 0) {
                try (ResultSet rs = preparedStatement.getGeneratedKeys()) {
                    if (rs.next()) {
                        user.setId(rs.getInt(1));
                    }
                }
            }
            connection.commit();
        } catch (SQLException e) {
            rollback(connection);
            throw new DBException(e.getMessage(), e);
        }finally {
            close(connection, preparedStatement);
        }
        return isInsert>0;
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String deleteFromUsers = DBRequest.DELETE_USERS_BY_LOGIN;
        int isDelete = 0;

        try {
            connection = DriverManager.getConnection(URL);
            for (User u : users) {
                preparedStatement = connection.prepareStatement(deleteFromUsers);
                preparedStatement.setString(1,u.getLogin());
                isDelete = preparedStatement.executeUpdate();
            }

        } catch (SQLException e) {
            throw new DBException("not delete users",e);
        } finally {
            close(connection,preparedStatement);
        }
        return isDelete > 1;
    }

    public User getUser(String login) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<User> users = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            preparedStatement = connection.prepareStatement(DBRequest.SELECT_USERS_BY_LOGIN);
            preparedStatement.setString(1, login);

            try (ResultSet resultSet = preparedStatement.executeQuery();) {
                if (resultSet.next ()) {
                    User user = new User (resultSet.getString("login"));
                    user.setId (resultSet.getInt ("id"));
                    user.setLogin (resultSet.getString ("login"));
                    users.add (user);

                }
            }
        } catch (SQLException e) {
            throw new DBException("ERROR get team", e);
        } finally {
            close(connection,preparedStatement);
        }
        if (users.size () == 0) {
            throw new DBException ("Login \"" + login + "\" not found");
        }

        return users.get (0);
    }

    public Team getTeam(String name) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<Team> teams = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
           preparedStatement = connection.prepareStatement(DBRequest.SELECT_TEAMS_WHERE_BY_NAME);
           preparedStatement.setString(1,name);
           try(ResultSet resultSet = preparedStatement.executeQuery();) {
               if (resultSet.next()) {
                   int team_id = resultSet.getInt(1);
                   String team_name = resultSet.getString(2);
                   Team team = new Team(team_name);
                   team.setId(team_id);
                   teams.add(team);

               }
           }
        } catch (SQLException e) {
            throw new DBException("ERROR SQL", e);
        } finally {
            close(connection,preparedStatement);
        }
        if (teams.size() == 0){
            throw new DBException("Login\""+name+"\" not found");
        }
        return teams.get(0);
    }

    public List<Team> findAllTeams() throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<Team> teamList = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            preparedStatement = connection.prepareStatement(DBRequest.SELECT_ALL_TEAMS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                Team team = new Team(name);
                team.setId(id);
                teamList.add(team);
            }

        } catch (SQLException e) {
            throw new DBException("ERROR SQL", e);
        } finally {
           close(connection,preparedStatement);
        }
        return teamList;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement stmt = null;
        try {
            connection = DriverManager.getConnection(URL);
            connection.setTransactionIsolation (Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit (false);
            stmt = connection.prepareStatement (DBRequest.INSERT_INTO_TEAMS_TABLE_ONE_VALUE, Statement.RETURN_GENERATED_KEYS);
            stmt.setString (1, team.getName ());
            int count = stmt.executeUpdate ();
            if (count > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys ()) {
                    if (rs.next ()) {
                        team.setId (rs.getInt (1));
                    }
                }
            }
            connection.commit ();
        } catch (SQLException e) {
            e.printStackTrace ();
            rollback (connection);
            throw new DBException ("Insert team error", e);
        } finally {
            close (connection,stmt);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException, RuntimeException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int resultBoolean = 0;

        try{
            connection = DriverManager.getConnection(URL);
            preparedStatement = connection.prepareStatement(DBRequest.SET_TEAMS_FOR_USER,Statement.NO_GENERATED_KEYS);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit(false);
            for (Team team : teams) {
                preparedStatement.setInt(1, user.getId());
                preparedStatement.setInt(2, team.getId());
                resultBoolean = preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(connection);
            throw new DBException("not setTeamsForUser update");
        }finally {
            close(connection,preparedStatement);
        }
        return resultBoolean>0;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<Team> teamList = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            preparedStatement = connection.prepareStatement(DBRequest.SELECT_TEAM_ID_BY_USER_ID);
            preparedStatement.setInt(1,user.getId());
            try(ResultSet resultSet = preparedStatement.executeQuery()){
               while (resultSet.next()){
                   int team_id = resultSet.getInt(2);
                    teamList.add(getTeamsByTeamId(team_id));
               }
            }
        }catch (SQLException e){
            throw new DBException("not found teams by user",e);
        }finally {
            close(connection,preparedStatement);
        }
        return teamList;
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int isDelete;

        try{
            connection = DriverManager.getConnection(URL);
            preparedStatement = connection.prepareStatement(DBRequest.DELETE_TEAMS_BY_TEAMS_NAME);
            preparedStatement.setString(1,team.getName());
            isDelete = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("don`t delete team");
        }
        return isDelete>0;
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int isUpdate;

        try{
            connection = DriverManager.getConnection(URL);
            preparedStatement = connection.prepareStatement(DBRequest.UPDATE_TEAM);
            preparedStatement.setString(1,team.getName());
            preparedStatement.setInt(2,team.getId());
            isUpdate = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("update failed");
        }finally {
            close(connection,preparedStatement);
        }
        return isUpdate>0;
    }

    public Team getTeamsByTeamId(int team_id) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Team team = null;
        try {
            connection = DriverManager.getConnection(URL);
            preparedStatement = connection.prepareStatement(DBRequest.SELECT_TEAMS_NAME_BY_TEAMS_ID);
            preparedStatement.setInt(1,team_id);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()){
                    team = new Team(resultSet.getString(2));
                    team.setId(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            throw new DBException("don`t found teams");
        }finally {
            close(connection, preparedStatement);
        }
        if (team == null){
            throw new DBException("value not found");
        }else {
            return team;
        }
    }

    private void rollback(Connection connection) {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }else {
            System.err.println("connection equals null");
        }
    }

    private void close(Connection connection, PreparedStatement preparedStatement) {
        if (preparedStatement != null){
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
