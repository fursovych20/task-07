package com.epam.rd.java.basic.task7.db;

public class DBRequest {
    public static final String SELECT_ALL_USERS = "SELECT * FROM users";
    public static final String INSERT_INTO_USERS_LOGIN_VALUES = "INSERT INTO users (login) VALUES  (?)";
    public static final String DELETE_USERS_BY_LOGIN = "DELETE FROM users WHERE login = (?)";
    public static final String SELECT_USERS_BY_LOGIN = "SELECT * FROM users WHERE login = (?)";
    public static final String SELECT_TEAMS_WHERE_BY_NAME = "SELECT * FROM teams WHERE teams.name = (?)";
    public static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
    public static final String INSERT_INTO_TEAMS_TABLE_ONE_VALUE = "INSERT INTO teams (teams.name) VALUES (?)";
    public static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    public static final String SELECT_TEAM_ID_BY_USER_ID = "SELECT * FROM users_teams WHERE user_id = ?";
    public static final String SELECT_TEAMS_NAME_BY_TEAMS_ID = "SELECT * FROM teams WHERE teams.id = ?";
    public static final String DELETE_TEAMS_BY_TEAMS_NAME = "DELETE FROM teams WHERE teams.name = ?";
    public static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
}
