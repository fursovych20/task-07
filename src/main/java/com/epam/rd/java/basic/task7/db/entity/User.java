package com.epam.rd.java.basic.task7.db.entity;

import lombok.EqualsAndHashCode;

import java.util.Objects;

@EqualsAndHashCode
public class User {
	private int id;

	private String login;

	public User(String login){
		this.login = login;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(login);
	}

	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object o) {
		return this.getLogin().equals(o.toString());
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, login);
	}
}
