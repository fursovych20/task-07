package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) throws DBException {
        DBManager dbManager = DBManager.getInstance();
        List<User> users = createAndInsertUsers(0, 5);
        List<Team> teams = createAndInsertTeams(0, 5);
        for (int j = 0; j < 5; j++) {
            dbManager.setTeamsForUser(users.get(j), teams.subList(0, j + 1).toArray(Team[]::new));
        }
    }

    private static List<Team> createAndInsertTeams(int from, int to) throws DBException {
        DBManager dbm = DBManager.getInstance();
        List<Team> teams = IntStream.range(from, to)
                .mapToObj(x -> "team" + x)
                .map(Team::createTeam)
                .collect(Collectors.toList());

        for (Team team : teams) {
            dbm.insertTeam(team);
        }
        return teams;
    }

    private static List<User> createAndInsertUsers(int from, int to) throws DBException {
        DBManager dbm = DBManager.getInstance();
        List<User> users = IntStream.range(from, to)
                .mapToObj(x -> "user" + x)
                .map(User::createUser)
                .collect(Collectors.toList());

        for (User user : users) {
            dbm.insertUser(user);
        }
        return users;
    }

    private static void print(List<?> list) {
        list.forEach(System.out::println);
    }
}
