package com.epam.rd.java.basic.task7.db.entity;

import lombok.EqualsAndHashCode;

import java.util.Objects;

@EqualsAndHashCode
public class Team {

	private int id;

	private String name;

	public Team(String name){
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		return this.getName().equals(o.toString());
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}
}
